#include <iostream>
#include <vector>
#include <cmath>
#include <math.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

void resol (double** a, double* x, double* b, int n, int* perm){

    //Creem un vector auxiliar per guardar les solucions de L*y = b
	double *y; 
	y = (double*) calloc(n, sizeof(double*));

    //Creem un vector al qual aplicarem el vector b permutat
	double *bb;
	bb = (double*) calloc(n, sizeof(double*));

    //Apliquem el vector de permutació a b
    for (int i = 0; i < n; ++i){ 
        bb[i] = b[perm[i]];
    }
    
    //Trobem y resolent el sistema L*y = b
    for (int i = 0; i < n; ++i) {
    	y[i] = bb[i];
    	for (int j = 0; j < i; ++j) y[i] = y[i] - a[i][j]*y[j];
    }

    //Trobem x resolent el sistema U*x = y
    x[n-1] = y[n-1]/a[n-1][n-1];
    for (int i = n - 2; i >= 0; --i) {
        for (int j = i+1; j < n; ++j) {
            y[i] = y[i] - a[i][j]*x[j];
            x[i] = y[i]/a[i][i];
        }
    }
}
