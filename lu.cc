#include <iostream>
#include <vector>
#include <cmath>
#include <math.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

void swap(int& a, int& b); //Intercanvia dos elements

void swap_files (double** a, int n, int fila1, int fila2); //Intercanvia dues files

int maximfila (double** a, int n, int j); //Troba la posició del màxim de cada fila

int lu(double** a, int n, int* perm, double tol){
    
    //Creem una variable que sigui el numero de permutacions en aplicar pivotatge parcial esglaonat
	int num_permutacions = 0;
    
    
	int h = 0;
	for (int k = 0; k < n; ++k){

		//Trobem la posició del millor pivot
		int millorpivot;
		int l = h;
		double max = -1;
		while (l < n){
			int maxim = maximfila (a, n, l);
			if (abs(a[l][k]/a[l][maxim]) > max){
				max = abs(a[l][k]/a[l][maxim]);
				millorpivot = l;
			}
			++l;
		}
		++h;

		//Si no fa falta intercanviar files no ho fem
		if (k != millorpivot){
			swap_files (a, n, k, millorpivot);
			swap (perm[k], perm[millorpivot]);
			++num_permutacions;
		}
        
        //Mirem que sigui un pivot adequat
        if (abs(a[k][k]) < tol) return 0;
        
		//Apliquem Gauss (LU)
		for (int i = k + 1; i < n; ++i){
			double multiplicador = a[i][k]/a[k][k];
			for (int j = k; j < n; ++j){
				a[i][j] -= multiplicador*a[k][j];
			}
            
            //Guardem els multiplicadors a la triangular inferior
			a[i][k] = multiplicador; 
		}
	}
    
    //Si el numero de permutacions és parell retornem 1
	if (num_permutacions%2 == 0) return 1;
    
    //Si el numero de permutacions és senar retornem -1
	return -1;
}
