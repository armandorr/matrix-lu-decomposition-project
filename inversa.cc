#include <iostream>
#include <vector>
#include <cmath>
#include <math.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

typedef vector<double> VI;

int lu(double** a, int n, int* perm, double tol); //Retorna 0 = No exitosa; 1 = Exitosa/Nombre parell de permutacions; -1 = Exitosa/Nombre senar de permutacions

int inversa(double** A, double** inv_a, int n, double* det_a, double tol){ 

    //Creem el vector de permutacio perm
    int *perm;
    perm = (int*) calloc(n, sizeof(int));

	//Omplim el perm ordenat
	for (int i = 0; i < n; ++i){ 
		perm[i] = i;
	}
    
    //Creem una copia de A per fer la descomposicio LU
    double **A_copia; 
    A_copia = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) A_copia[i] = (double *) calloc(n, sizeof(double));
    
    //Copiem la matriua A a la matriu A_copia
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j){
            A_copia[i][j] = A[i][j];
        }
    }
    
    //Descomposem A_copia en LU
    int LU = lu(A_copia, n, perm, tol);
    
    //Creem el vector e permutat
    VI ee(n);
    
	//Calcula A^-1
    for (int k = 0; k < n; ++k){ 
        
        //Creem el vector canonic amb un 1 a la posició k
        VI e(n,0);
        e[k] =  1;
        
        //Apliquem el vector de permutacio a el vector canonic
        for (int i = 0; i < n; ++i){ 
        ee[i] = e[perm[i]];
        }

        //Resol L*y = e; Trobem y i la posem a la primera columna d'inv_a
        for (int i = 0; i < n; ++i) {
            inv_a[i][k] = ee[i];
            for (int j = 0; j < i; ++j) inv_a[i][k] -= A_copia[i][j]*inv_a[j][k];
        }

        //Resol U*x = y. Trobem x i la posem a la primera columna d'inv_a
        inv_a[n-1][k] = inv_a[n-1][k]/A_copia[n-1][n-1];
        for (int i = n - 2; i >= 0; --i) {
            double aux = inv_a[i][k];
            for (int j = i+1; j < n; ++j) {
              aux -= A_copia[i][j]*inv_a[j][k];
                inv_a[i][k] = aux/A_copia[i][i];
            }
        }
    }
    
    //Calcula deteterminant d'A
	double diagonal_u = 1;

    //Calculem la multiplicació de la diagonal de A_copia (en LU)
	for (int i = 0; i < n; ++i){
		diagonal_u = diagonal_u*A_copia[i][i];
	}
	*(det_a) = diagonal_u;
    
    //Canviem, si fa falta, el signe del determinant
    if (LU == -1) *(det_a) = (-1.)*(*det_a);
    
    //Si el determinant es més petit que la tolerancia direm que A es singular i retornarem 0
	if (fabs(*det_a) < tol) return 0;
    
    //Si la matriu es regular retorna 1
	return 1;
}
