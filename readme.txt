﻿This program will calculate:
-The determinant of A (it will just show this information if it's considered a singular matrix (tolerance 10^-12))
-The permutation vector P that makes P*A = L*U
-The 1 and supremum norm of the residue P*A-L*U to check it's correctness
-The solution vector x that makes A*x = b (b was also given by the input)
-The 1 and supremum norm of the residue A*x-b to check it's correctness
-The inverse of A
-The 1 and supremum norm of the residue A*inv(A) to check it's correctness

It will also create a file with the solution x


To compile:

g++ -Wall -o LU main.cc resol.cc funcions.cc inversa.cc lu.cc


To execute: 

./LU M00.dat


-Made by Armando Rodriguez and Toni Buron, 2018