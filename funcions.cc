#include <iostream>
#include <vector>
#include <cmath>
#include <math.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

typedef vector<double> VI;

void swap(int& a, int& b) { //intercanvia dos elements
	int c = a;
	a = b;
	b = c;
}

void swap_files (double** a, int n, int fila1, int fila2) { //intercanvia dues files
	for (int i = 0; i < n; ++i){
		swap (a[fila1][i], a[fila2][i]);
	}
}

double norma_matricial_1 (double** a, int n) { //Norma matricial 1
    double max = -1;
    for (int i = 0; i < n; ++i) {
        double sum = 0;
        for (int j = 0; j < n; ++j){
        sum += abs(a[j][i]);
        }
        if (sum > max){
            max = sum;
        }
    }
    return max;
}

double norma_vectorial_1 (double* x, int n) {
    double sum = 0;
    for (int i = 0; i < n; ++i){
        sum += abs(x[i]);
    }
    return sum;
}

double norma_vectorial_inf (double* x, int n) { //maxim vector
    double max = -1;
    for (int i = 0; i < n; ++i){
        if (abs(x[i]) > max){
            max = abs(x[i]);
        }
    }
    return max;
}

double norma_matricial_inf (double** a, int n) { //Norma matricial infinit
    double max = -1;
    for (int i = 0; i < n; ++i) {
        double sum = 0;
        for (int j = 0; j < n; ++j){
        sum += abs(a[i][j]);
        }
        if (sum > max){
            max = sum;
        }
    }
    return max;
}

double *suma_vectors (double* x, double* y, int n) {
    double *c;
	c = (double*) calloc(n, sizeof(double)); 
    
    for (int i = 0; i < n; ++i){
        c[i] = x[i] + y[i];
    }
    return c;
}

double *resta_vectors (double* x, double* y, int n) {
    double *c;
	c = (double*) calloc(n, sizeof(double));
    
    for (int i = 0; i < n; ++i){
        c[i] = x[i] - y[i];
    }
    return c;
}

double **suma_matrius (double** A, double** B, int n) {
    
    double **AsumB; // Matriu AsumB
    AsumB = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) AsumB[i] = (double *) calloc(n, sizeof(double));

    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            AsumB[i][j] = A[i][j] + B[i][j];
        }
    }
    return AsumB;
}

double **resta_matrius (double** A, double** B, int n) {
    double **AresB;
    AresB = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) AresB[i] = (double *) calloc(n, sizeof(double));
    
    for (int i = 0; i < n; ++i){
        for (int j = 0; j < n; ++j){
            AresB[i][j] = A[i][j] - B[i][j];
        }
    }
    return AresB;
}

double **multiplica_matrius(double** A, double** B, int n, int m, int k) {
	// Declarem la matriu resultant del producte

    double **AB;
    AB = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) AB[i] = (double *) calloc(n, sizeof(double));

    for(int i = 0; i < n; ++i){
        for(int j = 0; j < k; ++j){
            for(int z = 0; z < m; ++z){
                AB[i][j] += A[i][z] * B[z][j];
			}
		}
	}
    return AB;
}

double *multiplica_matriu_per_vector(double** A, double* x, int n) {
    double *Ax;
	Ax = (double*) calloc(n, sizeof(double)); 

    for(int i = 0; i < n; ++i){
        for(int j = 0; j < n; ++j){
                Ax[i] += A[i][j] * x[j];
			}
		}
    return Ax;
}

int maximfila (double** a, int n, int j){ //troba el maxim de cada fila
	double maxim = -1;
	int filmax;
	for (int i = 0; i < n; ++i){
		if (abs(a[j][i]) > maxim){
			filmax = i;
			maxim = abs(a[j][i]);
		}
	}
	return filmax;
}
