#include <iostream>
#include <vector>
#include <cmath>
#include <math.h>
#include <fstream>
#include <stdlib.h>

using namespace std;

typedef vector<double> VI;

void swap(int& a, int& b); //Intercanvia dos elements

void swap_files (double** a, int n, int fila1, int fila2); //Intercanvia dues files

double norma_vectorial_1 (double* x, int n); //Norma vectorial 1

double norma_vectorial_inf (double* x, int n); //Norma vectorial infinit

double norma_matricial_1 (double** a, int n); //Norma matricial 1

double norma_matricial_inf (double** a, int n); //Norma matricial infinit

double *suma_vectors (double* x, double* y, int n); //Suma dos vectors

double *resta_vectors (double* x, double* y, int n); //Resta dos vectors
 
double **suma_matrius (double** A, double** B, int n); //Suma dues matrius
    
double **resta_matrius (double** A, double** B, int n); //Resta dues matrius

double **multiplica_matrius(double** A, double** B, int n, int m, int k); //Multiplica dues matrius

double *multiplica_matriu_per_vector(double** A, double* x, int n); //Multiplica matriu per vector

int maximfila (double** a, int n, int j); //Troba la posició del màxim de cada fila

int lu(double** a, int n, int* perm, double tol); //Retorna 0 = No exitosa; 1 = Exitosa/Nombre parell de permutacions; -1 = Exitosa/Nombre senar de permutacions

void resol (double** a, double* x, double* b, int n, int* perm); //Resol el sistema Ax = b

int inversa(double** A, double** inv_a, int n, double* det_a, double tol); //Retorna 0 = Matriu singular; 1 = Matriu regular


int main(int argc, char *argv[]){
	
	ifstream fileIn;

	//Obrim el fitxer per llegir les dades
    if (argc >= 2) fileIn.open(argv[1]); 
	else {cerr << "ERROR: no hi ha fitxer d'entrada" << endl; return 0;}
    
	//Comprovem que s'hagi pogut llegir el fitxer de dades
    if (!fileIn.is_open()) {
        cerr << "Error: no s'ha pogut obrir el fitxer " << argv[1]
            << " per llegir les dades" << endl;
        return 0;
    }
    
	//Llegim dimensió de la matriu
    int n;
    fileIn >> n;	

	//Creem la matriu A
    double **A;
    A = (double **) calloc(n, sizeof(double));
    for (int i = 0; i < n; ++i) A[i] = (double *) calloc(n, sizeof(double)); 

	//Copia de A per a treballar
    double **a; 
    a = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) a[i] = (double *) calloc(n, sizeof(double));
    
	//Creem vector b
    double *b; 
	b = (double*) calloc(n, sizeof(double)); 
	
	//Llegim components no nul·les de A
    int m;
    fileIn >> m;

    //Llegim les m components de A
    int i, j;
    for (int k = 0; k < m; k++) { 
        fileIn >> i >> j;
        fileIn >> A[i][j];
        a[i][j] = A[i][j];
    }
    
    //Llegim components no nul·les de b
    int k;
    fileIn >> k; 

    //Llegim les k components de b
    int l;
    for (int i = 0; i < k; ++i) {
        fileIn >> l;
        fileIn >> b[l];
    }
    
	//Tanquem el fitxer d'entrada
    fileIn.close();
    
	//Començem a treballar
    
	//Creem el vector de les solucions x
	double *x; 
	x = (double*) calloc(n, sizeof(double)); 
	
	//Creem el vector de permutació
    int *perm;
    perm = (int*) calloc(n, sizeof(int));

	//Omplim el perm ordenat
	for (int i = 0; i < n; ++i){ 
		perm[i] = i;
	}
	
	//Assignem un valor a la tolerancia admesa
	double tol = pow(10, -12); 
    
	//Retorna 0 = No exitosa; 1 = Exitosa/Nombre parell de permutacions; -1 = Exitosa/Nombre senar de permutacions
	//Retorna la matriu a descomposada en LU i el vector de permutació
	int LU = lu(a, n, perm, tol); 
    
	//Si la descomposició no ha estat exitosa es perquè algun dels pivots era més petit que la tol admesa, per tant aturem el programa
    if (LU == 0) {
        cout << endl <<  "A is a singular matrix" << endl << endl;
        return 0;
    }
    
	//Resol el sistema Ax = b passant-ho a LUx = Pb;
	//Retorna el vector de solucions x
	resol (a, x, b, n, perm); 
    
	//Creem el determinant de A
    double* det_a;
    *(det_a) = 1.;
    
	//Creem la matriu inversa de A
	double **inv_a; 
    inv_a = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) inv_a[i] = (double *) calloc(n, sizeof(double));
    
	//Retorna 0 = Matriu singular; 1 = Matriu regular
	//Retorna la matriu inversa de A i el determinant de A
	int func_inversa = inversa (A, inv_a, n, det_a, tol); 
    
	//Si la matriu surt singular, ho indiquem i aturem el programa
    if (func_inversa == 0) {
        cout << endl << "A is a singular matrix" << endl << endl;
        return 0;
    }
    
	//Creem la matriu PA (matriu A amb el vector de permutació aplicat)
    double **PA; 
    PA = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) PA[i] = (double *) calloc(n, sizeof(double));

	//Apliquem permutació per files a A per la guardem a PA
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            PA[i][j] = A[perm[i]][j];
        }
    }
    
	//Creem la matriu L 
    double **L; 
    L = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) L[i] = (double *) calloc(n, sizeof(double));

	//Fem que L sigui la part triangular inferior de a amb 1's a la diagonal
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j <= i; ++j) {
            L[i][j] = a[i][j];
        }
        L[i][i] = 1;
    }
    
	//Creem la matriu U
    double **U;
    U = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) U[i] = (double *) calloc(n, sizeof(double));

	//Fem que U sigui la part triangular superior de a
    for (int j = 0; j < n; ++j){
        for (int i = 0; i <= j; ++i){
            U[i][j] = a[i][j];
        }
    }
    
	//Creem la matriu L*U
    double **LperU; 
    LperU = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) LperU[i] = (double *) calloc(n, sizeof(double));
    
	//Fem que la matriu LperU sigui la mutiplicació de la L i la U
    LperU = multiplica_matrius(L, U, n, n, n);
    
	//Creem la matriu PA-LU
    double **PAmenysLU; 
    PAmenysLU = (double **) calloc(n, sizeof(double)); 
    for (int i = 0; i < n; ++i) PAmenysLU[i] = (double *) calloc(n, sizeof(double));
    
	//Fem que la matriu PAmenysLU sigui la resta de PA i L*U
    PAmenysLU = resta_matrius(PA, LperU, n);
    
	//Calculem les normes de PA-LU
    double PALU1 = norma_matricial_1(PAmenysLU, n);
    double PALUI = norma_matricial_inf(PAmenysLU, n);
    
	//Creem el vector Ax-b
    double *SAxb; 
	SAxb = (double*) calloc(n, sizeof(double));
    
	//Fem que el vector SAxb sigui la resta de A*x i b
    SAxb = resta_vectors(multiplica_matriu_per_vector(A, x, n), b, n);
    
	//Calculem les normes de Ax-b
    double SAxb1 = norma_vectorial_1 (SAxb, n);
    double SAxbI = norma_vectorial_inf (SAxb, n);
    
	//Reutilitzem la matriu L i la fem la identitat
    for (int i = 0; i < n; ++i){ 
    	for (int j = 0; j < n; ++j){
    	if (j == i) L[i][j] = 1;
    	else L[i][j] = 0;
    	}
	}

	//Reutilitzem la matriu U i fem que sigui la resta de A*inv_a i L
    U = resta_matrius(multiplica_matrius(A, inv_a, n, n, n), L, n);

	//Calculem les normes de (A*A^-1)-Id
    double AxInv1 = norma_matricial_1(U, n);
    double AxInvI = norma_matricial_inf(U, n);
    
    //Fem la sortida per pantalla de les solucions
    cout << endl;
    cout << "Data input file: " << argv[1] << endl;
    cout << "Dimension n=" << n << endl << endl;
    cout << "Determinant of A: " << *(det_a) << endl << endl;
    cout << "Permutation vector:" << endl << "[" << perm[0];
    for (int i = 1; i < n; ++i){
        cout << "," << perm[i];
    }
    cout << "]" << endl << endl;
    cout << "Checking the norm of the residue: |PA-LU|" << endl;
    cout << "   1-norm: " << PALU1 << endl;
    cout << "   supremum norm: " << PALUI << endl << endl;
    cout << "Solving Ax=b:" << endl;
    cout << "   1-norm of the residue vector Ax-b: " << SAxb1 << endl;
    cout << "   supremum norm of the residue vector Ax-b: " << SAxbI << endl << endl;
    cout << "We compute the inverse of A:" << endl;
    cout << "   1-norm of A*inv(A) - I: " << AxInv1 << endl;
    cout << "   supremum norm of A*inv(A) - I: " << AxInvI << endl << endl;

	//Fitxer de sortida
    ofstream fileOut("solucioSL.dat");
    
	//Fem que tregui les solucions amb format expoencial
    fileOut.setf(ios::scientific);
    fileOut.precision(10);
    
    //Escrivim al fitxer de sortida el vector de solucions x
    for (int i = 0; i < n; ++i){
		fileOut << i << " " << x[i] << endl; 
	}

	// Tanquem el fitxer de sortida
	fileOut.close();

    return 0;
}
